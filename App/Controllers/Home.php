<?php

namespace App\Controllers;

class Home extends \App\Controller
{
    
    public function index()
    {
        session_start();
        //добавление задачи
    	$name_user=htmlentities($_POST['name'], ENT_QUOTES, ' UTF-8 ');
    	$email=$_POST['email'];
    	$task_text=htmlentities($_POST['task'], ENT_QUOTES, ' UTF-8 ');
    	if(!empty($name_user)&&!empty($email)&&!empty($task_text)&&filter_var($email, FILTER_VALIDATE_EMAIL))
    		$message=$this->add_task($name_user,$email,$task_text);
    	else if($_POST['add_task'])
    		$message="Задача не добавлена. Заполните корректно все поля!";

        //пагинация
        $tasks_per_page=3;
        $this_page=intval($_GET['page']);
        $tasks_count=\App\Models\Task::get_count();
        $pages_count=ceil($tasks_count/$tasks_per_page);
        if(empty($this_page)||$this_page>$pages_count || $this_page<0)$this_page=0;
        else $this_page--;
        $first_task_id=$this_page*$tasks_per_page;

        //сортировка
        $order=$_GET['order'];
        $type_sort= ($order=='status') ? 'DESC' : 'ASC';
        if($order!='status'&&$order!='name'&&$order!='email')$order=null;

        //вывод задач
        $tasks=\App\Models\Task::get_tasks($first_task_id,$tasks_per_page,$order, $type_sort);
        return $this->render('Home', ['tasks'=>$tasks, 'message'=>$message, 'page'=>$this_page, 'pages_count'=>$pages_count, 'order'=>$order]);        
    }
    public function add_task($name,$email,$task)
    {    	
    	$result=\App\Models\Task::add([$name,$email,$task]);
    	if($result) $message="Задача успешно добавлена"; 
    	return $message;
    }
    
}