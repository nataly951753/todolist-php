<?php

namespace App\Controllers;

class Logout extends \App\Controller
{    
    public function index()
    {             
        session_start(); 
    	$_SESSION=array();
        header("Location: home"); exit;
             
    }
}