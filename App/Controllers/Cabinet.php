<?php

namespace App\Controllers;

class Cabinet extends \App\Controller
{    
    public function index()
    {        
        session_start();
        if(isset($_SESSION['username'])){
        	header("Location: home"); exit;
        }
    	$login=htmlentities($_POST['login'], ENT_QUOTES, ' UTF-8 ');
    	$password=htmlentities($_POST['password'], ENT_QUOTES, ' UTF-8 ');
    	//авторизация
        $admin=\App\Models\Admin::first($login);
        if($admin)
        	if(password_verify($password,$admin[0]['password'])){
				$_SESSION['username']=$login;
        		header("Location: home"); exit;        		
        	}
        header("Location: login"); exit;
    }
    public function change()
    {  
        session_start();
        if(isset($_SESSION['username']) && \App\Models\Admin::first($_SESSION['username'])){
        	$text_task=htmlentities($_POST['task'], ENT_QUOTES, ' UTF-8 ');
        	$status_task=(int)isset($_POST['status']);
        	$id_task=intval($_POST['id']);
        	if(!empty($text_task)&&!empty($id_task)){
        		\App\Models\Task::change_task($id_task,$text_task,$status_task);
            }
            header("Location: ..\home"); exit;
    	}
    	header("Location: ..\login"); exit;
    }
}