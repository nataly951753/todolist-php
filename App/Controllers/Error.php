<?php

namespace App\Controllers;

class Error extends \App\Controller
{
    
    public function error ()
    {        
        return $this->render('error');        
    }
    
}