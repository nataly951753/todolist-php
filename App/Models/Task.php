<?php

namespace App\Models;

class Task extends \App\Model
{
    protected static $table='users_tasks';
    protected static $column=[
    		'name',
    		'email',
    		'task'
    ];
    
    
    public static function get_tasks($start,$end,$orderby, $type_sort){
    	return \App::$db->getData( null, static::$table, $start, $end, $orderby, $type_sort);
    }
    public static function change_task($id,$task,$status){
        return \App::$db->updateValue(['task'=>$task,'status'=>$status], static::$table, ["id",$id]);
    }
    

    
}