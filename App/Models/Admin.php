<?php

namespace App\Models;

class Admin extends \App\Model
{
    protected static $table='admins';
    protected static $column=[
    		'login',
    		'password'
    ];

    
    public static function first($login){
        return \App::$db->getData( ["login" , $login], static::$table);
    }
}