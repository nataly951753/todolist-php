<?php

class App

{
        
    public static $router;
    
    public static $db;
    
    public static $kernel;
    
    public static function init()
    {
        set_exception_handler(['App','handleException']);
        spl_autoload_register(['static','loadClass']);
        error_reporting(E_ERROR);
        static::bootstrap();        
    }
    
    public static function bootstrap()
    {
        static::$router = new App\Router();
        static::$kernel = new App\Kernel();
        static::$db = new App\Db();       
    }
    
    public static function loadClass ($className)
    {      
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        include_once ROOTPATH.DIRECTORY_SEPARATOR.$className.'.php';   
    }
    
    public function handleException (Throwable $e){
        echo $e;
        echo static::$kernel->launchAction('Error', 'error', [$e]);          
    }
    
}