<?php

namespace App;

use App;

class Db 
{

    public $pdo;
    
    public function __construct()
    {
        try {
            $settings = $this->getPDOSettings();
            $this->pdo = new \PDO($settings['dsn'], $settings['user'], $settings['pass'], null);
        } 
        catch (PDOException $e) {
             echo 'Подключение не удалось: ' . $e->getMessage();
             return 0;
        }           
    }
    
    protected function getPDOSettings()
    {
        
        $config = include ROOTPATH.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'Db.php';
        $result['dsn'] = "{$config['type']}:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}";
        $result['user'] = $config['user'];
        $result['pass'] = $config['pass'];
        return $result;       
    }
    //генерация INSERT по имени таблицы, колонкам и добавляемым значениям
    public function setValue($ColumnName=array(), $TableName, $Values=array()){ 
        $columns = '`'.implode('`, `',$ColumnName).'`';
        $sql="INSERT INTO $TableName ($columns)";
        $values = substr(str_repeat('?,',count($Values)),0,-1);
        $sql.=" VALUES($values)";
        $prepared=$this->pdo->prepare($sql);
        $result=$prepared->execute($Values);
        return $result;
    }   
    //генерация SELECT по имени таблицы, колонкам и добавляемым значениям с ORDER BY и LIMIT
    public function getData($Condition=[],$TableName, $start=null,$end=null,$orderby=null, $type_sort='ASC'){
        $sql='SELECT *';
        $sql.=" FROM $TableName";
        if(count($Condition)>0)$sql.=" WHERE $Condition[0] = ?";         
        if(!is_null($orderby))$sql.=" ORDER BY $orderby $type_sort";
        if(!is_null($start))$sql.=" LIMIT $start";
        if(!is_null($end))$sql.=", $end";
        $result=$this->pdo->prepare($sql); 
        $result->execute(array($Condition[1])); 
        return $result->fetchAll();
    }
    public function updateValue($ColumnNamesAndValues=array(), $TableName, $Condition=[]){
        $columns = implode('=?, ',array_keys($ColumnNamesAndValues)).'=?';
        $sql="UPDATE $TableName SET $columns ";
        $sql.=" WHERE $Condition[0] = ?";
        $result=$this->pdo->prepare($sql); 
        $result->execute(array_merge(array_values($ColumnNamesAndValues),array($Condition[1]))); 
        return $result->fetchAll();
    }
    public function getDataCount( $TableName){
        $sql='SELECT COUNT(*) as total FROM '.$TableName;
        $result=$this->pdo->query($sql)->fetch();
        return $result['total'];
    }    
    
}