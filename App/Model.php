<?php

namespace App;

use App;

class Model 
{        
    protected static $table;  
    protected static $column; 

    public static function add($values){
    	return \App::$db->setValue(static::$column, static::$table, $values);
    } 
    public static function get_count(){
        return \App::$db->getDataCount(static::$table);
    }
}