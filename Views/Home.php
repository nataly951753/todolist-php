<?php 	include_once 'Components/Message.php';?>
<?php 	include_once 'Components/NewTask.php';?>

<h2 class="text-center">Задачи</h2>
<?php 	include_once 'Components/Sort.php';?>

<?php foreach ($tasks as $task) { 
	if(isset($_SESSION['username']))
		include 'Components/TaskAdmin.php';
	else
		include 'Components/Task.php';
 }	 ?>
<div class="row justify-content-center">
<?php 	include_once 'Components/Pagination.php';?>
</div>


