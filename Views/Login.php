<h1 class="text-center">Авторизация</h1>
<div class="row justify-content-center">
	<div class="col-6">
<form action='..\cabinet' method='post'>
  <div class="form-group">
    <label for="login">Логин</label>
    <input type="text" class="form-control" id="login"  name="login">
  </div>
  <div class="form-group">
    <label for="password">Пароль</label>
    <input type="password" class="form-control" id="password" name="password" >
  </div>
  <button type="submit" class="btn btn-primary">Войти</button>
</form>
</div>
</div>
