<div class="row justify-content-center">		
	<div class="col-10">
		<div class="card my-3 bg-light">
			<div class="card-body">
		<h5 class="card-title"><?=htmlentities($task['name'])?>
		<h6 class="card-subtitle mb-2 text-muted"><?=htmlentities($task['email'])?></h6>
		<form action='..\cabinet/change' method='post'>		
		<div class="form-group">
		<label for="task_text">Задача</label>
		<textarea class="form-control" id="task" name="task" rows="6"><?=htmlentities($task['task'])?></textarea>
		</div>
		<div class="form-group form-check">
			<?php if($task['status']){ ?>
		    <input type="checkbox" class="form-check-input" id="status" name="status" checked>
			<?php } else{ ?>
		    <input type="checkbox" class="form-check-input" id="status" name="status">
			<?php } ?>
		    <label class="form-check-label" for="status">Выполнено</label>
		</div>
		<input type="hidden" name="id" value="<?=$task['id']?>">
		<button type="submit" class="btn btn-primary">Изменить задачу</button>
		</form>
			</div>
		</div>
	</div>
</div>