<nav aria-label="page-navigation">
  <ul class="pagination">
    <?php if(!$page) { ?>
    <li class="page-item disabled">
      <?php }  else { ?>
    <li class="page-item ">
      <?php } $href="..\?page=".$page; 
      ($order) ? $href.="&order=$order" : false ;
      ?>
      <a class="page-link" href="<?=$href?>" tabindex="-1" aria-disabled="true">Предыдущие</a>
    </li>
    <?php 
   for ($i=1; $i <= $pages_count; $i++){
     if ($page+1==$i) {
     ?>
     <li class="page-item active" aria-current="page">
  <?php } else{ ?>
    <li class="page-item">
  <?php } $href="..\?page=".$i; 
   ($order) ? $href.="&order=$order" : false ;
  ?>
      <a class="page-link" href="<?=$href?>"><?=$i ?></a></li>
  <?php }
    $href="..\?page=".($page+2);
   ($order) ? $href.="&order=$order" : false ;
    if($page+1==$pages_count) {
  ?>
    <li class="page-item disabled">
    <?php }  else { ?>
    <li class="page-item ">
      <?php } ?>
      <a class="page-link" href="<?=$href?>">Следующие</a>
    </li>
  </ul>
</nav>