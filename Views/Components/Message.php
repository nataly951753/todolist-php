<div class="row justify-content-center">
	<div class="col-9 mt-5">	
		<?php 	
		if($message){ 
		?>
			<div class="alert alert-info alert-dismissible fade show" role="alert">
			  <?=$message?>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>				
		<?php 	} ?>
	</div>
</div>