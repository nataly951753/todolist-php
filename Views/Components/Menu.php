<nav class="navbar navbar-light cast-color">
	<div class="container">
		<a class="navbar-brand" href="..\">Tasks</a>
		<?php 
        if(isset($_SESSION['username'])){
		 ?>
		<a class="btn btn-outline-primary px-5" href="..\logout" role="button">Выход</a>
		<?php }
		else { ?>
		<a class="btn btn-outline-primary px-5" href="..\login" role="button">Вход</a>
		<?php } ?>
	</div>
</nav>