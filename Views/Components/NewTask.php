<div class="row justify-content-center my-4">
	<a class="btn  btn-block btn-primary btn-lg" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" >
		Создать задачу
	</a>
</div>
<div class="row justify-content-center bg-light">		
	<div class="col-9 my-5 collapse" id="collapseExample">
		<form action='..\home' method='post'>
		<div class="form-group">
		<label for="name">Имя</label>
		<input type="text" class="form-control" id="name" name="name" placeholder="Иванов И.И." maxlength="255">
		</div>	
		<div class="form-group">
		<label for="email">Email</label>
		<input type="email" class="form-control" aria-describedby="emailHelp" id="email" name="email" placeholder="name@example.com" maxlength="255">
		</div>	
		<div class="form-group">
		<label for="task_text">Задача</label>
		<textarea class="form-control" id="task" name="task" rows="6"></textarea>
		</div>
		<input type="hidden" name="add_task" value="true">
		<button type="submit" class="btn btn-primary">Добавить задачу</button>
		</form>
	</div>
</div>