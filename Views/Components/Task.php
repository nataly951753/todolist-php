<div class="row justify-content-center">		
	<div class="col-10">
		<div class="card my-3 bg-light">
			<div class="card-body">
				<h5 class="card-title"><?=htmlentities($task['name'])?> 
				<?php if($task['status']){ ?>
				<span class="badge badge-success">Завершена!</span>
				<?php } ?>
				</h5>
				<h6 class="card-subtitle mb-2 text-muted"><?=htmlentities($task['email'])?></h6>
				<p class="card-text"><?=htmlentities($task['task'])?></p>
			</div>
		</div>
	</div>
</div>