<?php

define('ROOTPATH', __DIR__);

require ROOTPATH.'/App/App.php';

App::init();
App::$kernel->launch();